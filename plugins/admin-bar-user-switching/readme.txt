=== Admin Bar User Switching ===
Contributors: wpmarkuk
Donate link: http://markwilkinson.me/saythanks/
Tags: users, user switching
Requires at least: 3.1
Tested up to: 4.0.1
Stable tag: 0.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Extends the excellent User Switching plugin by John Blackbourn by adding a User Switching link to the admin bar.

== Description ==

An admin bar “Switch to User” options is provided which on hover lists the users on the site which you can switch to. Best for sites with less than 20 users!

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload `admin-bar-user-witching` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

Nothing so far!

== Screenshots ==
N/A

== Changelog ==

= 0.2 =
* Fix menu ID bug whereby if user has not added first name and last name they are not included in the user switching list - thanks to @rarst for finding this!

= 0.1 =
Initial release.

== Upgrade Notice ==
Update through the WordPress admin as notified.
