<?php

/**
 * Multisite support for the page builder.
 *
 * @class FLBuilderMultisite
 */

final class FLBuilderMultisite {

    /** 
     * @method init
     */
    static public function init()
    {
        add_action('wpmu_new_blog', 'FLBuilderMultisite::install_for_new_blog', 10, 6);
        add_filter('wpmu_drop_tables', 'FLBuilderMultisite::uninstall_on_delete_blog');
    }

    /** 
     * @method install
     */
    static public function install() 
    {
        global $blog_id;
        global $wpdb;
        
        $original_blog_id   = $blog_id;
        $blog_ids           = $wpdb->get_col("SELECT blog_id FROM $wpdb->blogs");
        
        foreach($blog_ids as $id) {
            switch_to_blog($id);
            FLBuilderAdmin::install();
        }
        
        switch_to_blog($original_blog_id);
    }

    /** 
     * @method install
     */
    static public function install_for_new_blog($blog_id, $user_id, $domain, $path, $site_id, $meta) 
    {
        global $wpdb;
        
        if(is_plugin_active_for_network('fl-builder/fl-builder.php')) {
            switch_to_blog($blog_id);
            FLBuilderAdmin::install();
            restore_current_blog();
        }
    }

    /** 
     * @method install
     */
    static public function uninstall() 
    {
        global $blog_id;
        global $wpdb;
        
        $original_blog_id   = $blog_id;
        $blog_ids           = $wpdb->get_col("SELECT blog_id FROM $wpdb->blogs");
        
        foreach($blog_ids as $id) {
            switch_to_blog($id);
            FLBuilderAdmin::uninstall();
        }
        
        switch_to_blog($original_blog_id);
    }

    /** 
     * @method uninstall_on_delete_blog
     */
    static public function uninstall_on_delete_blog($tables) 
    {
        return $tables;
    }
}