<?php if($settings->title_size == 'custom') : ?>
.fl-builder-content .fl-cta-title {
    font-size: <?php echo $settings->title_custom_size; ?>px;
}
<?php endif; ?>
<?php if(!empty($settings->text_color)) : ?>
.fl-node-<?php echo $id; ?> {
    color: #<?php echo $settings->text_color; ?>;
}
.fl-node-<?php echo $id; ?> * {    
    color: #<?php echo $settings->text_color; ?>;
}
<?php endif; ?>
<?php if(!empty($settings->bg_color)) : ?>
.fl-node-<?php echo $id; ?> .fl-module-content {
    background-color: #<?php echo $settings->bg_color; ?>;
    background-color: rgba(<?php echo implode(',', FLBuilderColor::hex_to_rgb($settings->bg_color)) ?>, <?php echo $settings->bg_opacity/100; ?>);
}
<?php endif; ?>
<?php if(is_numeric($settings->spacing)) : ?>
.fl-node-<?php echo $id; ?> .fl-module-content { 
    padding: <?php echo $settings->spacing; ?>px;
}
<?php endif; ?>
<?php

FLBuilder::render_module_css('button', $id, array(
    'align'             => '',
    'bg_color'          => $settings->btn_bg_color,
    'bg_hover_color'    => $settings->btn_bg_hover_color,
    'border_radius'     => $settings->btn_border_radius,
    'font_size'         => $settings->btn_font_size,
    'link'              => $settings->btn_link,
    'link_target'       => $settings->btn_link_target,
    'padding'           => $settings->btn_padding,
    'text'              => $settings->btn_text,
    'text_color'        => $settings->btn_text_color,
    'text_hover_color'  => $settings->btn_text_hover_color,
    'three_d'           => $settings->btn_3d,
    'width'             => $settings->layout == 'stacked' ? 'auto' : 'full'
));