<?php

if(class_exists('FLUpdater')) {
    FLUpdater::add_product(array(
        'name'      => 'Beaver Builder Theme', 
        'version'   => '1.1.8', 
        'slug'      => 'bb-theme',
        'type'    	=> 'theme'
    ));
}