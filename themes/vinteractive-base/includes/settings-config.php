<?php

FLAdmin::register_settings_forms(array(
    'presets'  => array(
    	'title'    => __('Skin', 'fl-automator'),
    	'description' => __('Get started by selecting a skin for your theme. Customize it further using the settings in the design section.', 'fl-automator'),
    	'sections' => array(
            'general'   => array(
            	'title'     => '',
            	'fields'    => array(
                    'skin'             => array(
                        'type'              => 'hidden',
                        'default'			=> 'default'
                    ),
                    'preset'           => array(
                        'type'              => 'hidden',
                        'default'			=> 'default'
                    )
                )
            )
        )
    ),
    'design'   => array(
        'title'    => __('Design', 'fl-automator'),
        'sections' => array(
            'general'   => array(
                'title'     => __('General', 'fl-automator'),
                'fields'    => array(
                    'layout'            => array(
                        'type'              => 'select',
                        'label'             => __('Layout', 'fl-automator'),
                        'default'           => 'full-width',
                        'options'           => array(
                            'boxed'             => __('Boxed', 'fl-automator'),
                            'full-width'        => __('Full Width', 'fl-automator')
                        )
                    ),
                    'accent_color'      => array(
                        'type'              => 'color-picker',
                        'label'             => __('Accent Color', 'fl-automator'),
                        'default'           => '428bca',
                        'help'				=> __('This will be used by your chosen skin to color elements such as links and buttons.', 'fl-automator')
                    )
                )
            ),
            'text'      => array(
                'title'     => __('Text', 'fl-automator'),
                'fields'    => array(
                    'heading_font'      => array(
                        'type'              => 'font',
                        'label'             => __('Heading Font', 'fl-automator'),
                        'default'           => 'Helvetica',
                        'weight'            => 'heading_weight',
                        'help-after'        => '<a href="http://www.google.com/fonts" target="_blank">'. __('Browse Google Fonts', 'fl-automator') .' &raquo;</a>'
                    ),
                    'heading_weight'    => array(
                        'type'              => 'select',
                        'options'           => array(),
                        'label'             => __('Heading Font Weight', 'fl-automator'),
                        'default'           => '300'
                    ),
                    'heading_color'     => array(
                        'type'              => 'color-picker',
                        'label'             => __('Heading Text Color', 'fl-automator'),
                        'default'           => '333333'
                    ),
                    'text_font'         => array(
                        'type'              => 'font',
                        'label'             => __('Body Font', 'fl-automator'),
                        'default'           => 'Roboto',
                        'help-after'        => '<a href="http://www.google.com/fonts" target="_blank">'. __('Browse Google Fonts', 'fl-automator') .' &raquo;</a>'
                    ),
                    'text_color'        => array(
                        'type'              => 'color-picker',
                        'label'             => __('Body Text Color', 'fl-automator'),
                        'default'           => '808080'
                    )
                )
            ),
            'body_bg' 	=> array(
                'title'     => __('Body Background', 'fl-automator'),
                'fields'    => array(
                    'bg_color'          => array(
                        'type'              => 'color-picker',
                        'label'             => __('Color', 'fl-automator'),
                        'default'           => 'f2f2f2'
                    ),
                    'bg_image'          => array(
                        'type'              => 'select',
                        'label'             => __('Image', 'fl-automator'),
                        'options'           => array(
                            ''                  => __('None', 'fl-automator'),
                            'custom'            => __('Custom', 'fl-automator')
                        ),
                        'toggle'            => array(
                            'custom'            => array(
                                'fields'            => array('bg_custom_image', 'bg_repeat', 'bg_position', 'bg_attachment', 'bg_size')
                            )
                        )
                    ),
                    'bg_custom_image'   => array(
                        'type'              => 'image',
                        'label'             => __('Image File', 'fl-automator')
                    ),
                    'bg_repeat'         => array(
                        'type'              => 'select',
                        'label'             => __('Repeat', 'fl-automator'),
                        'default'           => 'tile',
                        'options'           => array(
                            'no-repeat'         => __('None', 'fl-automator'),
                            'repeat'            => __('Tile', 'fl-automator'),
                            'repeat-x'          => __('Horizontal', 'fl-automator'),
                            'repeat-y'          => __('Vertical', 'fl-automator')
                        )
                    ),
                    'bg_position'       => array(
                        'type'              => 'select',
                        'label'             => __('Position', 'fl-automator'),
                        'default'           => 'center top',
                        'options'           => array(
                            'left top'          => __('Left Top', 'fl-automator'),
                            'left center'       => __('Left Center', 'fl-automator'),
                            'left bottom'       => __('Left Bottom', 'fl-automator'),
                            'right top'         => __('Right Top', 'fl-automator'),
                            'right center'      => __('Right Center', 'fl-automator'),
                            'right bottom'      => __('Right Bottom', 'fl-automator'),
                            'center top'        => __('Center Top', 'fl-automator'),
                            'center center'     => __('Center Center', 'fl-automator'),
                            'center bottom'     => __('Center Bottom', 'fl-automator')
                        )
                    ),
                    'bg_attachment'     => array(
                        'type'              => 'select',
                        'label'             => __('Attachment', 'fl-automator'),
                        'default'           => 'scroll',
                        'options'           => array(
                            'scroll'            => __('Scroll', 'fl-automator'),
                            'fixed'             => __('Fixed', 'fl-automator')
                        )
                    ),
                    'bg_size'           => array(
                        'type'              => 'select',
                        'label'             => __('Scale', 'fl-automator'),
                        'options'           => array(
                            'auto'              => __('None', 'fl-automator'),
                            'contain'           => __('Fit', 'fl-automator'),
                            'cover'             => __('Fill', 'fl-automator')
                        )
                    )
                )
            ),
            'content_bg' => array(
                'title'     => __('Content Background', 'fl-automator'),
                'fields'    => array(
                    'content_bg_color'  => array(
                        'type'              => 'color-picker',
                        'label'             => __('Color', 'fl-automator'),
                        'default'           => 'ffffff'
                    )
				)
            ),
            'top_bar_bg' => array(
                'title'     => __('Top Bar Background', 'fl-automator'),
                'fields'    => array(
                	'top_bar_bg_type'   => array(
                        'type'              => 'select',
                        'label'             => __('Color', 'fl-automator'),
                        'default'           => 'content',
                        'options'           => array(
                            'none'              => __('None', 'fl-automator'),
                            'content'           => __('Same as Content', 'fl-automator'),
                            'custom'            => __('Custom', 'fl-automator')
                        ),
                        'toggle'            => array(
                            'content'           => array(
                                'fields'        	=> array('top_bar_bg_grad')
                            ),
                            'custom'            => array(
                                'fields'        	=> array('top_bar_bg_color', 'top_bar_bg_grad')
                            )
                        )
                    ),
                    'top_bar_bg_color'  => array(
                        'type'              => 'color-picker',
                        'label'             => __('Custom Color', 'fl-automator'),
                        'default'           => '',
                        'show_reset'		=> true
                    ),
                    'top_bar_bg_grad'   => array(
                        'type'              => 'select',
                        'label'             => __('Gradient', 'fl-automator'),
                        'default'           => '0',
                        'options'           => array(
                            '0'              	=> __('Disabled', 'fl-automator'),
                            '1'           		=> __('Enabled', 'fl-automator')
                        )
                    )
				)
            ),
            'header_bg' => array(
                'title'     => __('Header Background', 'fl-automator'),
                'fields'    => array(
                	'header_bg_type'   => array(
                        'type'              => 'select',
                        'label'             => __('Color', 'fl-automator'),
                        'default'           => 'content',
                        'options'           => array(
                            'none'              => __('None', 'fl-automator'),
                            'content'           => __('Same as Content', 'fl-automator'),
                            'custom'            => __('Custom', 'fl-automator')
                        ),
                        'toggle'            => array(
                            'content'           => array(
                                'fields'        	=> array('header_bg_grad')
                            ),
                            'custom'            => array(
                                'fields'        	=> array('header_bg_color', 'header_bg_grad')
                            )
                        )
                    ),
                    'header_bg_color'   => array(
                        'type'              => 'color-picker',
                        'label'             => __('Custom Color', 'fl-automator'),
                        'default'           => '',
                        'show_reset'		=> true
                    ),
                    'header_bg_grad'    => array(
                        'type'              => 'select',
                        'label'             => __('Gradient', 'fl-automator'),
                        'default'           => '0',
                        'options'           => array(
                            '0'              	=> __('Disabled', 'fl-automator'),
                            '1'           		=> __('Enabled', 'fl-automator')
                        )
                    )
				)
            ),
            'nav_bg' 	=> array(
                'title'     => __('Menu Background', 'fl-automator'),
                'fields'    => array(
                	'nav_bg_type'   	=> array(
                        'type'              => 'select',
                        'label'             => __('Color', 'fl-automator'),
                        'default'           => 'content',
                        'options'           => array(
                            'none'              => __('None', 'fl-automator'),
                            'content'           => __('Same as Content', 'fl-automator'),
                            'custom'            => __('Custom', 'fl-automator')
                        ),
                        'toggle'            => array(
                            'content'           => array(
                                'fields'        	=> array('nav_bg_grad')
                            ),
                            'custom'            => array(
                                'fields'        	=> array('nav_bg_color', 'nav_bg_grad')
                            )
                        )
                    ),
                    'nav_bg_color'  	=> array(
                        'type'              => 'color-picker',
                        'label'             => __('Custom Color', 'fl-automator'),
                        'default'           => '',
                        'show_reset'		=> true
                    ),
                    'nav_bg_grad'   	=> array(
                        'type'              => 'select',
                        'label'             => __('Gradient', 'fl-automator'),
                        'default'           => '0',
                        'options'           => array(
                            '0'              	=> __('Disabled', 'fl-automator'),
                            '1'           		=> __('Enabled', 'fl-automator')
                        )
                    )
				)
            ),
            'footer_widgets_bg' => array(
                'title'     => __('Footer Widgets Background', 'fl-automator'),
                'fields'    => array(
                	'footer_widgets_bg_type' => array(
                        'type'              => 'select',
                        'label'             => __('Color', 'fl-automator'),
                        'default'           => 'content',
                        'options'           => array(
                            'none'              => __('None', 'fl-automator'),
                            'content'           => __('Same as Content', 'fl-automator'),
                            'custom'            => __('Custom', 'fl-automator')
                        ),
                        'toggle'            => array(
                            'custom'            => array(
                                'fields'        	=> array('footer_widgets_bg_color')
                            )
                        )
                    ),
                    'footer_widgets_bg_color' => array(
                        'type'              => 'color-picker',
                        'label'             => __('Custom Color', 'fl-automator'),
                        'default'           => '',
                        'show_reset'		=> true
                    )
				)
            ),
            'footer_bg' => array(
                'title'     => __('Footer Background', 'fl-automator'),
                'fields'    => array(
                	'footer_bg_type' 	=> array(
                        'type'              => 'select',
                        'label'             => __('Color', 'fl-automator'),
                        'default'           => 'content',
                        'options'           => array(
                            'none'              => __('None', 'fl-automator'),
                            'content'           => __('Same as Content', 'fl-automator'),
                            'custom'            => __('Custom', 'fl-automator')
                        ),
                        'toggle'            => array(
                            'custom'            => array(
                                'fields'        	=> array('footer_bg_color')
                            )
                        )
                    ),
                    'footer_bg_color' => array(
                        'type'              => 'color-picker',
                        'label'             => __('Custom Color', 'fl-automator'),
                        'default'           => '',
                        'show_reset'		=> true
                    )
				)
            )
        )
    ),
    'top_bar' => array(
        'title'    => __('Top Bar', 'fl-automator'),
        'sections' => array(
            'general'   => array(
                'title'     => __('General', 'fl-automator'),
                'fields'    => array(
                	'top_bar_layout'   => array(
                        'type'              => 'select',
                        'default'           => 'none',
                        'label'             => __('Layout', 'fl-automator'),
                        'options'           => array(
                            'none'              => __('None', 'fl-automator'),
                            '1-col'             => __('1 Column', 'fl-automator'),
                            '2-cols'            => __('2 Columns', 'fl-automator')
                        ),
                        'toggle'            => array(
                            '1-col'             => array(
                                'sections'          => array('top_bar_col1', 'top_bar_bg')
                            ),
                            '2-cols'            => array(
                                'sections'          => array('top_bar_col1', 'top_bar_col2', 'top_bar_bg')
                            )
                        )
                    )
                )
			),
            'top_bar_col1' => array(
                'title'     => __('Top Bar Column 1', 'fl-automator'),
                'fields'    => array(
                    'top_bar_col1_layout' => array(
                        'type'              => 'select',
                        'default'           => 'text',
                        'label'             => __('Layout', 'fl-automator'),
                        'options'           => array(
                            'text'              => __('Text', 'fl-automator'),
                            'social'            => __('Social Icons', 'fl-automator'),
                            'menu'              => __('Menu', 'fl-automator')
                        ),
                        'toggle'            => array(
                            'text'              => array(
                                'fields'            => array('top_bar_col1_text')
                            )
                        )
                    ),
                    'top_bar_col1_text' => array(
                        'type'              => 'text',
                        'default'           => '',
                        'label'             => __('Text', 'fl-automator')
                    )
                )
            ),
            'top_bar_col2' => array(
                'title'     => __('Top Bar Column 2', 'fl-automator'),
                'fields'    => array(
                    'top_bar_col2_layout' => array(
                        'type'              => 'select',
                        'default'           => 'menu',
                        'label'             => __('Layout', 'fl-automator'),
                        'options'           => array(
                            'text'              => __('Text', 'fl-automator'),
                            'social'            => __('Social Icons', 'fl-automator'),
                            'menu'              => __('Menu', 'fl-automator')
                        ),
                        'toggle'            => array(
                            'text'              => array(
                                'fields'            => array('top_bar_col2_text')
                            )
                        )
                    ),
                    'top_bar_col2_text' => array(
                        'type'              => 'text',
                        'default'           => '',
                        'label'             => __('Text', 'fl-automator')
                    )
                )
            )
		)
    ),
    'header' => array(
        'title'    => __('Header', 'fl-automator'),
        'sections' => array(
            'general'   => array(
                'title'     => __('General', 'fl-automator'),
                'fields'    => array(
                    'fixed_header'  	=> array(
                        'type'              => 'select',
                        'default'           => 'visible',
                        'options'			=> array(
                        	'visible'			=> __('Enabled', 'fl-automator'),
                        	'hidden'			=> __('Disabled', 'fl-automator')
                        ),
                        'label'             => __('Fixed Header', 'fl-automator'),
                        'help'				=> __('Show a fixed header bar as the page is scrolled.', 'fl-automator')
                    )
                )
            ),
            'logo' 		=> array(
                'title'     => __('Logo', 'fl-automator'),
                'fields'    => array(
                	'logo_type'         => array(
                        'type'              => 'select',
                        'default'           => 'text',
                        'label'             => __('Type', 'fl-automator'),
                        'options'           => array(
                            'text'              => __('Text', 'fl-automator'),
                            'image'             => __('Image', 'fl-automator')
                        ),
                        'toggle'            => array(
                            'text'              => array(
                                'fields'            => array('logo_text', 'logo_font', 'logo_weight', 'logo_size')
                            ),
                            'image'             => array(
                                'fields'            => array('logo_image')
                            )
                        )
                    ),
                	'logo_text'         => array(
                        'type'              => 'text',
                        'default'           => get_bloginfo('name'),
                        'label'             => __('Text', 'fl-automator')
                    ),
                    'logo_font'      	=> array(
                        'type'              => 'font',
                        'label'             => __('Font', 'fl-automator'),
                        'default'           => __('Helvetica', 'fl-automator'),
                        'weight'            => 'logo_weight',
                        'help-after'        => '<a href="http://www.google.com/fonts" target="_blank">'. __('Browse Google Fonts', 'fl-automator') .' &raquo;</a>'
                    ),
                    'logo_weight'    	=> array(
                        'type'              => 'select',
                        'options'           => array(),
                        'label'             => __('Font Weight', 'fl-automator'),
                        'default'           => '300'
                    ),
                    'logo_image'        => array(
                        'type'              => 'image',
                        'label'             => __('Image', 'fl-automator')
                    ),
                	'logo_size'         => array(
                        'type'              => 'text',
                        'default'           => '40',
                        'label'             => __('Size', 'fl-automator'),
                        'help-after'		=> 'px',
                        'size'				=> 'small'
                    )
                )
            ),
            'menu' 		=> array(
                'title'     => __('Menu', 'fl-automator'),
                'fields'    => array(
                	'nav_position'      => array(
                        'type'              => 'select',
                        'default'           => 'right',
                        'label'             => __('Position', 'fl-automator'),
                        'options'           => array(
                            'bottom'            => __('Bottom', 'fl-automator'),
                            'right'             => __('Right', 'fl-automator'),
                            'centered'          => __('Centered', 'fl-automator')
                        ),
                        'toggle'            => array(
                            'bottom'            => array(
                                'sections'          => array('header_content', 'nav_bg')
                            ),
                            'centered'          => array(
                                'sections'          => array('nav_bg')
                            )
                        )
                    ),
                    'nav_search'  		=> array(
                        'type'              => 'select',
                        'default'           => 'visible',
                        'options'			=> array(
                        	'visible'			=> __('Enabled', 'fl-automator'),
                        	'hidden'			=> __('Disabled', 'fl-automator')
                        ),
                        'label'             => __('Search Icon', 'fl-automator')
                    )
                )
            ),
            'header_content' => array(
                'title'     => __('Content', 'fl-automator'),
                'fields'    => array(
                    'header_content'   => array(
                        'type'              => 'select',
                        'default'           => 'social-text',
                        'label'             => __('Layout', 'fl-automator'),
                        'options'           => array(
                            'none'              => __('None', 'fl-automator'),
                            'text'              => __('Text', 'fl-automator'),
                            'social'            => __('Social Icons', 'fl-automator'),
                            'social-text'       => __('Text &amp; Social Icons', 'fl-automator')
                        ),
                        'toggle'            => array(
                            'text'              => array(
                                'fields'            => array('header_text')
                            ),
                            'social-text'       => array(
                                'fields'            => array('header_text')
                            )
                        )
                    ),
                    'header_text'      => array(
                        'type'              => 'text',
                        'default'           => __('Call Us! 1-800-555-5555', 'fl-automator'),
                        'label'             => __('Text', 'fl-automator')
                    )
                )
            )
        )
    ),
    'footer' => array(
        'title'    => __('Footer', 'fl-automator'),
        'sections' => array(
            'footer' => array(
                'title'     => __('Footer', 'fl-automator'),
                'fields'    => array(
                	'show_footer_widgets' => array(
                        'type'              => 'select',
                        'default'           => 'all',
                        'label'             => __('Footer Widgets', 'fl-automator'),
                        'options'           => array(
                            'disabled'          => __('Disabled', 'fl-automator'),
                            'all'               => __('All Pages', 'fl-automator'),
                            'home'              => __('Homepage Only', 'fl-automator')
                        ),
                        'toggle'            => array(
                            'all'              => array(
                                'sections'          => array('footer_widgets_bg')
                            ),
                            'home'             => array(
                                'sections'          => array('footer_widgets_bg')
                            )
                        )
                    ),
                    'footer_layout'    => array(
                        'type'              => 'select',
                        'default'           => '1-col',
                        'label'             => __('Footer Layout', 'fl-automator'),
                        'options'           => array(
                            'none'              => __('None', 'fl-automator'),
                            '1-col'             => __('1 Column', 'fl-automator'),
                            '2-cols'            => __('2 Columns', 'fl-automator')
                        ),
                        'toggle'            => array(
                            '1-col'             => array(
                                'sections'          => array('footer_col1', 'footer_bg')
                            ),
                            '2-cols'            => array(
                                'sections'          => array('footer_col1', 'footer_col2', 'footer_bg')
                            )
                        )
                    )
                )
            ),
            'footer_col1' => array(
                'title'     => __('Footer Column 1', 'fl-automator'),
                'fields'    => array(
                    'footer_col1_layout' => array(
                        'type'              => 'select',
                        'default'           => 'text',
                        'label'             => __('Layout', 'fl-automator'),
                        'options'           => array(
                            'text'              => __('Text', 'fl-automator'),
                            'social'            => __('Social Icons', 'fl-automator'),
                            'social-text'       => __('Text &amp; Social Icons', 'fl-automator'),
                            'menu'              => __('Menu', 'fl-automator')
                        ),
                        'toggle'            => array(
                            'text'              => array(
                                'fields'            => array('footer_col1_text')
                            ),
                            'social-text'       => array(
                                'fields'            => array('footer_col1_text')
                            )
                        )
                    ),
                    'footer_col1_text' => array(
                        'type'              => 'text',
                        'default'           => '',
                        'label'             => __('Text', 'fl-automator')
                    )
                )
            ),
            'footer_col2' => array(
                'title'     => __('Footer Column 2', 'fl-automator'),
                'fields'    => array(
                    'footer_col2_layout' => array(
                        'type'              => 'select',
                        'default'           => 'text',
                        'label'             => __('Layout', 'fl-automator'),
                        'options'           => array(
                            'text'              => __('Text', 'fl-automator'),
                            'social'            => __('Social Icons', 'fl-automator'),
                            'social-text'       => __('Text &amp; Social Icons', 'fl-automator'),
                            'menu'              => __('Menu', 'fl-automator')
                        ),
                        'toggle'            => array(
                            'text'              => array(
                                'fields'            => array('footer_col2_text')
                            ),
                            'social-text'       => array(
                                'fields'            => array('footer_col2_text')
                            )
                        )
                    ),
                    'footer_col2_text' => array(
                        'type'              => 'text',
                        'default'           => '1-800-555-5555 &bull; <a href="mailto:info@mydomain.com">info@mydomain.com</a>',
                        'label'             => __('Text', 'fl-automator')
                    )
                )
            )
        )
    ),
    'social' => array(
        'title'    => __('Social', 'fl-automator'),
        'sections' => array(
            'display'   => array(
                'title'     => __('Display', 'fl-automator'),
                'fields'    => array(
                    'social_color'      => array(
                        'type'              => 'select',
                        'label'             => __('Color', 'fl-automator'),
                        'default'           => 'mono',
                        'options'           => array(
                            'branded'           => __('Branded', 'fl-automator'),
                            'mono'              => __('Monochrome', 'fl-automator')
                        )
                    )
                )
            ),
            'links'   => array(
                'title'     => __('Links', 'fl-automator'),
                'fields'    => array(
                    'facebook'          => array(
                        'type'              => 'text',
                        'label'             => __('Facebook', 'fl-automator')
                    ),
                    'twitter'           => array(
                        'type'              => 'text',
                        'label'             => __('Twitter', 'fl-automator')
                    ),
                    'google'            => array(
                        'type'              => 'text',
                        'label'             => __('Google', 'fl-automator')
                    ),
                    'linkedin'          => array(
                        'type'              => 'text',
                        'label'             => __('LinkedIn', 'fl-automator')
                    ),
                    'yelp'              => array(
                        'type'              => 'text',
                        'label'             => __('Yelp', 'fl-automator')
                    ),
                    'pinterest'         => array(
                        'type'              => 'text',
                        'label'             => __('Pinterest', 'fl-automator')
                    ),
                    'tumblr'            => array(
                        'type'              => 'text',
                        'label'             => __('Tumblr', 'fl-automator')
                    ),
                    'vimeo'             => array(
                        'type'              => 'text',
                        'label'             => __('Vimeo', 'fl-automator')
                    ),
                    'youtube'           => array(
                        'type'              => 'text',
                        'label'             => __('YouTube', 'fl-automator')
                    ),
                    'flickr'            => array(
                        'type'              => 'text',
                        'label'             => __('Flickr', 'fl-automator')
                    ),
                    'instagram'         => array(
                        'type'              => 'text',
                        'label'             => __('Instagram', 'fl-automator')
                    ),
                    'dribbble'          => array(
                        'type'              => 'text',
                        'label'             => __('Dribbble', 'fl-automator')
                    ),
                    '500px'             => array(
                        'type'              => 'text',
                        'label'             => __('500px', 'fl-automator')
                    ),
                    'blogger'           => array(
                        'type'              => 'text',
                        'label'             => __('Blogger', 'fl-automator')
                    ),
                    'github'            => array(
                        'type'              => 'text',
                        'label'             => __('GitHub', 'fl-automator')
                    ),
                    'rss'               => array(
                        'type'              => 'text',
                        'label'             => __('RSS', 'fl-automator')
                    )
                )
            )
        )
    ),
    'blog' => array(
        'title'    => __('Blog', 'fl-automator'),
        'sections' => array(
            'general'   => array(
                'title'     => __('General', 'fl-automator'),
                'fields'    => array(
                    'blog_layout'       => array(
                        'type'              => 'select',
                        'default'           => 'sidebar-right',
                        'label'             => __('Layout', 'fl-automator'),
        				'help'				=> __('The location of the blog sidebar.', 'fl-automator'),
                        'options'           => array(
                            'sidebar-right'     => __('Sidebar Right', 'fl-automator'),
                            'sidebar-left'      => __('Sidebar Left', 'fl-automator'),
                            'no-sidebar'        => __('No Sidebar', 'fl-automator')
                        ),
                        'toggle'            => array(
                            'sidebar-right'     => array(
                                'fields'            => array('blog_sidebar_size')
                            ),
                            'sidebar-left'      => array(
                                'fields'            => array('blog_sidebar_size')
                            )
                        )
                    ),
                    'blog_sidebar_size' => array(
                        'type'              => 'select',
                        'default'           => '4',
                        'label'             => __('Sidebar Size', 'fl-automator'),
                        'options'           => array(
                            '4'                 => __('Large', 'fl-automator'),
                            '3'                 => __('Medium', 'fl-automator'),
                            '2'                 => __('Small', 'fl-automator')
                        )
                    ),
                    'blog_show_author'  => array(
                        'type'              => 'select',
                        'default'           => 'visible',
                        'label'             => __('Post Author', 'fl-automator'),
                        'options'			=> array(
                        	'visible'			=> __('Visible', 'fl-automator'),
                        	'hidden'			=> __('Hidden', 'fl-automator')
                        )
                    ),
                    'blog_show_date'  	=> array(
                        'type'              => 'select',
                        'default'           => 'visible',
                        'label'             => __('Post Date', 'fl-automator'),
                        'options'			=> array(
                        	'visible'			=> __('Visible', 'fl-automator'),
                        	'hidden'			=> __('Hidden', 'fl-automator')
                        )
                    )
                )
            ),
            'archives'  => array(
                'title'     => __('Archive Pages', 'fl-automator'),
                'fields'    => array(
                    'blog_show_full'  	=> array(
                        'type'              => 'select',
                        'default'           => '0',
                        'label'             => __('Show Full Text', 'fl-automator'),
                        'options'			=> array(
                        	'1'					=> __('Yes', 'fl-automator'),
                        	'0'					=> __('No', 'fl-automator')
                        ),
                        'help'				=> __('Whether or not to show the entire post on archive pages. If no, the excerpt will be shown.', 'fl-automator')
                    ),
                    'blog_show_thumbs'  => array(
                        'type'              => 'select',
                        'default'           => 'beside',
                        'label'             => __('Featured Image', 'fl-automator'),
                        'options'           => array(
                            ''                  => __('Hidden', 'fl-automator'),
                            'above'             => __('Above Posts', 'fl-automator'),
                            'beside'            => __('Beside Posts', 'fl-automator')
                        )
                    )
                )
            ),
            'single'  => array(
                'title'     => __('Single Post Pages', 'fl-automator'),
                'fields'    => array(
                    'blog_show_cats'  	=> array(
                        'type'              => 'select',
                        'default'           => 'visible',
                        'label'             => __('Categories', 'fl-automator'),
                        'options'			=> array(
                        	'visible'			=> __('Visible', 'fl-automator'),
                        	'hidden'			=> __('Hidden', 'fl-automator')
                        )
                    ),
                    'blog_show_tags'  	=> array(
                        'type'              => 'select',
                        'default'           => 'visible',
                        'label'             => __('Tags', 'fl-automator'),
                        'options'			=> array(
                        	'visible'			=> __('Visible', 'fl-automator'),
                        	'hidden'			=> __('Hidden', 'fl-automator')
                        )
                    )
                )
            )
        )
    ),
    'woocommerce' => array(
        'title'    => __('WooCommerce', 'fl-automator'),
        'sections' => array(
            'general'   => array(
                'title'     => __('General', 'fl-automator'),
                'fields'    => array(
                    'woo_layout'        => array(
                        'type'              => 'select',
                        'default'           => 'no-sidebar',
                        'label'             => __('Layout', 'fl-automator'),
        				'help'				=> __('The location of the WooCommerce sidebar.', 'fl-automator'),
                        'options'           => array(
                            'sidebar-right'     => __('Sidebar Right', 'fl-automator'),
                            'sidebar-left'      => __('Sidebar Left', 'fl-automator'),
                            'no-sidebar'        => __('No Sidebar', 'fl-automator')
                        ),
                        'toggle'            => array(
                            'sidebar-right'     => array(
                                'fields'            => array('woo_sidebar_size')
                            ),
                            'sidebar-left'      => array(
                                'fields'            => array('woo_sidebar_size')
                            )
                        )
                    ),
                    'woo_sidebar_size'  => array(
                        'type'              => 'select',
                        'default'           => '4',
                        'label'             => __('Sidebar Size', 'fl-automator'),
                        'options'           => array(
                            '4'                 => __('Large', 'fl-automator'),
                            '3'                 => __('Medium', 'fl-automator'),
                            '2'                 => __('Small', 'fl-automator')
                        )
                    )
                )
            ),
            'categories' => array(
                'title'     => __('Category Pages', 'fl-automator'),
                'fields'    => array(
                    'woo_cats_add_button' => array(
                        'type'              => 'select',
                        'default'           => 'hidden',
                        'label'             => __('Add to Cart Button', 'fl-automator'),
        				'help'		        => __('Show the Add to Cart button on category pages?', 'fl-automator'),
                        'options'			=> array(
                        	'visible'			=> __('Visible', 'fl-automator'),
                        	'hidden'			=> __('Hidden', 'fl-automator')
                        )
                    )
                )
            )
        )
    ),
    'code' => array(
        'title'    => __('Code', 'fl-automator'),
        'sections' => array(
            'css'       => array(
                'title'     => __('CSS', 'fl-automator'),
                'fields'    => array(
                    'css'               => array(
                        'type'              => 'code',
                        'editor'			=> 'css',
                        'label'             => '',
                        'rows'				=> '15'
                    )
                )
            ),
            'js'        => array(
                'title'     => __('JavaScript', 'fl-automator'),
                'fields'    => array(
                    'js'                => array(
                        'type'              => 'code',
                        'editor'			=> 'javascript',
                        'label'             => '',
                        'rows'				=> '15'
                    )
                )
            ),
            'head'      => array(
                'title'     => __('Head', 'fl-automator'),
                'fields'    => array(
                    'head'              => array(
                        'type'              => 'code',
                        'editor'			=> 'html',
                        'label'             => '',
                        'rows'				=> '15'
                    )
                )
            )
        )
    ),
    'more' => array(
        'title'    => __('More', 'fl-automator'),
        'sections' => array(
            'general'   => array(
                'title'     => __('Browser', 'fl-automator'),
                'fields'    => array(
                    'favicon'           => array(
                        'type'              => 'image',
        				'help'				=> __('You may specify an icon to be used by the browser in the URL bar. This image must be 16x16 and in accepted file formats.', 'fl-automator'),
        				'help-after'        => '<a href="http://codex.wordpress.org/Creating_a_Favicon" target="_blank">'. __('Learn More', 'fl-automator') .' &raquo;</a>',
                        'label'             => __('Favicon Image', 'fl-automator')
                    ),
                    'lightbox'          => array(
                        'type'              => 'select',
                        'label'             => __('Lightbox', 'fl-automator'),
                        'default'           => 'enabled',
                        'options'           => array(
                            'enabled'           => __('Enabled', 'fl-automator'),
                            'disabled'          => __('Disabled', 'fl-automator')
                        ),
                        'help'				=> __('Disable the built in lightbox if you are using a custom lightbox for your site.', 'fl-automator'),
                    )
                )
            ),
            'export' => array(
                'title'     => __('Export Settings', 'fl-automator'),
                'fields'    => array(
                    'export'            => array(
                        'type'              => 'button',
                        'value'				=> __('Download Export File', 'fl-automator'),
        				'help'				=> __('Download an export file of your theme settings.', 'fl-automator'),
                        'label'             => __('Export', 'fl-automator')
                    )
                )
            ),
            'import' => array(
                'title'     => __('Import Settings', 'fl-automator'),
                'fields'    => array(
                    'import_file'       => array(
                        'type'              => 'file',
        				'help'				=> __('Upload an export file to import your settings.', 'fl-automator'),
                        'label'             => __('Import File', 'fl-automator')
                    ),
                    'import'            => array(
                        'type'              => 'button',
                        'value'				=> __('Upload Import File', 'fl-automator'),
                        'label'             => '&nbsp;'
                    )
                )
            )
        )
    )
));