<div class="fl-page-footer-text">
    <span>&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?></span>
    <span> | </span>
    <span id="fl-site-credits-toggle"><a href="javascript:void(0);" onclick="this.style.display = 'none'; document.getElementById('fl-site-credits').style.display = 'inline';"><?php _e('Credits', 'fl-automator'); ?></a></span>
    <span id="fl-site-credits"><a href="http://www.wpbeaverbuilder.com" target="_blank" title="WordPress Page Builder Themes">WordPress Page Builder Themes</a> by Beaver Builder</span>
    <script type="text/javascript">document.getElementById('fl-site-credits').style.display = 'none';</script>
</div>