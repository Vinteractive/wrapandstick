<article class="fl-post" id="fl-post-<?php the_ID(); ?>" itemscope="itemscope" itemtype="http://schema.org/BlogPosting">

	<header class="fl-post-header">
		<h2 class="fl-post-title" itemprop="headline">
		    <?php the_title(); ?>
		    <?php edit_post_link(__('Edit', 'fl-automator')); ?>
        </h2>
        <?php FLTheme::post_top_meta(); ?>
    </header><!-- .fl-post-header -->
    
    <div class="fl-post-content clearfix" itemprop="text">
        <?php 
        
        the_content(__('<span class="fl-post-more">Read more &raquo;</span>', 'fl-automator'));
        
        wp_link_pages(array(
            'before' => '<div class="fl-post-page-nav">' . __('Pages:', 'fl-automator'), 
            'after' => '</div>', 
            'next_or_number' => 'number'
        ));
        
        ?>
    </div><!-- .fl-post-content -->
	
	<?php FLTheme::post_bottom_meta(); ?>
	<?php comments_template(); ?>
	
</article>
<!-- .fl-post -->