<?php

/**
 * Helper class for admin options.
 *
 * @class FLAdmin
 */
final class FLAdmin {
	
	/**
     * @property $forms
     */
	static public $forms = array();
	
	/**
     * @property $messages
     */
	static public $messages = array();
	
	/**
     * @property $settings
     */
	static public $settings = null;
	
	/**
     * @property $settings_key
     */
	static public $settings_key = 'fl_theme_settings';
	
	/**
     * @property $skin_key
     */
	static public $skin_key = 'fl_theme_skin_id';
	
	/**
     * @method init
     */
	static public function init() 
	{
        FLAdmin::styles();
        FLAdmin::scripts();
        FLAdmin::update();
        
		// Do an export?
        if(isset($_GET['fl-export']) && current_user_can('manage_options')) {
            self::export();
        }
	}
    
    /** 
     * @method update
     */
    static public function update()
    {
        $version = get_site_option('_fl_automator_version');
        
        if($version == '{FL_THEME_VERSION}') {
            return;
        }
        if(version_compare($version, FL_THEME_VERSION, '=')) {
            return;
        }      
    
        self::clear_skin_cache();
        self::compile_skin();
        
        update_site_option('_fl_automator_version', FL_THEME_VERSION);
    }
		
	/**
     * @method register_settings_forms
     */
	static public function register_settings_forms($data = array()) 
	{
        self::$forms = $data;
	}
	
	/**
     * @method clear_settings
     */
	static public function clear_settings() 
	{
        delete_option(self::$settings_key);
	}
	
	/**
     * @method menu
     */
	static public function menu() 
	{
		add_submenu_page('themes.php', __('Theme Settings', 'fl-automator'), __('Theme Settings', 'fl-automator'), 'edit_theme_options', 'fl-theme-settings', 'FLAdmin::render_settings_page');
	}
	
	/**
     * @method styles
     */	 
	static public function styles()
	{
		global $pagenow;
		

        if(isset($_REQUEST['page']) && $_REQUEST['page'] == 'fl-theme-settings') {
            wp_enqueue_style('fl-color-picker', FL_THEME_URL . '/css/colorpicker.css');
			wp_enqueue_style('fl-theme-settings', FL_THEME_URL . '/css/settings.css');
        }
        if($pagenow == 'themes.php' && !isset($_REQUEST['page'])) {
	        wp_enqueue_style('fl-theme-admin', FL_THEME_URL . '/css/admin.css');
        }
	}
	 
	/**
     * @method scripts
     */	 
	static public function scripts()
	{
        if(isset($_REQUEST['page']) && $_REQUEST['page'] == 'fl-theme-settings') {
			wp_enqueue_media();
			wp_enqueue_script('fl-color-picker', FL_THEME_URL . '/js/colorpicker.js');
			wp_enqueue_script('ace', FL_THEME_URL . '/js/ace/ace.js');
			wp_enqueue_script('fl-theme-settings', FL_THEME_URL . '/js/settings.js');
        }
	}
	
	/**
     * @method render_settings_page
     */	 
	static public function render_settings_page()
	{
		self::save_settings();
		
		$settings = self::get_settings();
		
		include FL_THEME_DIR . '/includes/settings.php';
	}
	
	/**
     * @method render_settings_menu
     */
	static public function render_settings_menu() 
	{
		foreach(self::$forms as $id => $form) {
		
            if($id == 'woocommerce' && !self::is_plugin_active('woocommerce')) {
                continue;
            }
		
			echo '<li><a href="#' . $id .'">'. $form['title'] .'</a></li>' . "\n";
		}
	}
	
	/**
     * @method save_settings
     */	
	static public function save_settings()
	{
        if(!isset($_POST['_fl_update_settings_nonce'])) {
            return;
        }
        elseif(wp_verify_nonce($_POST['_fl_update_settings_nonce'], '_fl_update_settings')) {
        
        	// Do an import instead?
        	if(!empty($_FILES['import_file']['name'])) {
	        	
	        	$settings = self::get_import_settings();
	        	
	        	if(!$settings) {
		        	return;
	        	}
        	}
        	else {
	            $settings = (object)$_POST;	        	
        	}
            
            // Clean the settings.
            foreach($settings as $key => $value) {
                $settings->$key = htmlspecialchars(htmlspecialchars_decode(stripslashes($value)));
            }

            // Update the database.
            update_option(self::$settings_key, json_encode($settings));
        
            // Clear old skins from the cache.
            self::clear_skin_cache();
            
            // Compile the skin.
            self::compile_skin();
            
			// Success!		
			if(!empty($_FILES['import_file']['name'])) {
				self::$messages[] = array(
		        	'type'	  => 'updated',
		        	'message' => __('Settings imported!', 'fl-automator')
		        );
			}
			else {
	            self::$messages[] = array(
	            	'type'	  => 'updated',
	            	'message' => __('Settings Saved!', 'fl-automator')
	            );				
			}
        }
	}
	
	/**
     * @method get_settings
     */	
	static public function get_settings()
	{
        // We don't have settings yet, get them from the database.
        if(!self::$settings) {
        
        	// Get preview settings.
        	if(self::is_preview()) {
	        	$settings = self::get_preview_settings();
        	}
        	
        	// Get saved settings.
        	else {
        	
	        	// Get the settings.
	            $settings = json_decode(get_option(self::$settings_key));
	            
	            // Merge default settings.
	            $settings = self::merge_settings('default', $settings);	
        	}
        	
        	// No settings! Get defaults.
        	if(!$settings) {
        		$settings = self::get_default_settings();
        	}
	            
            // Clean the settings.
            foreach($settings as $key => $value) {
                $settings->$key = htmlspecialchars_decode($value);
            }
        }
        // We have cached the settings already.
        else {
            $settings = self::$settings;
        }
        
        return $settings;
	}
	
	/**
     * @method get_default_settings
     */	
	static public function get_default_settings()
	{
		$settings = new stdClass();
		
		foreach(self::$forms as $form) {
            foreach($form['sections'] as $section) {
            	if(!isset($section['fields'])) {
                	continue;
            	}
                foreach($section['fields'] as $field_id => $field) {
                    $default = isset($field['default']) ? $field['default'] : '';
                    $settings->$field_id = $default;
                }
            }
        }
        
        return $settings;
	}
	
	/**
     * @method get_preview_settings
     */	
	static public function get_preview_settings()
	{
		if(self::is_preview()) {
		
			include FL_THEME_DIR . '/includes/presets.php';

			$preset  						= $fl_presets[$_GET['fl-preview']];
			$preset['settings']['skin'] 	= $preset['skin'];
			$preset['settings']['preset'] 	= $_GET['fl-preview'];
			
			if(current_user_can('manage_options')) {
				return self::merge_settings('saved', (object)$preset['settings']);
			}
			elseif(self::is_demo_server()) {
				return self::merge_settings('default', (object)$preset['settings']);
			}
			
			return false;
		}
	}
	
	/**
     * @method is_preview
     */	
	static public function is_preview()
	{
		include FL_THEME_DIR . '/includes/presets.php';

		if(!isset($_GET['fl-preview']) || !isset($fl_presets[$_GET['fl-preview']])) {
			return false;
		}
		elseif(current_user_can('manage_options') || self::is_demo_server()) {
			return true;
		}
		
		return false;
	}
	
	/**
     * @method is_demo_server
     */	
	static public function is_demo_server()
	{
        return stristr($_SERVER['HTTP_HOST'], 'demos.wpbeaverbuilder.com');
	}
	
	/**
     * @method merge_settings
     */	
	static public function merge_settings($merge_with = 'default', $settings = null)
	{
		if(!$settings) {
			return false;
		}
		elseif($merge_with == 'default') {
			$new_settings = self::get_default_settings();
		}
		elseif($merge_with == 'saved') {
			$new_settings = json_decode(get_option(self::$settings_key));
			$new_settings = self::merge_settings('default', $new_settings);
		}
		
		foreach($settings as $field_id => $field) {
			$new_settings->$field_id = $field;
		}
		
		return $new_settings;
	}
	
	/**
     * @method clear_skin_cache
     */	
	static public function clear_skin_cache()
	{
	    $cache_dir = self::get_cache_dir();
	    
	    if(!empty($cache_dir['path']) && stristr($cache_dir['path'], 'fl-automator')) {
	    
    	    $skins = glob($cache_dir['path'] . '*');
            
            foreach($skins as $skin) {
                if(is_file($skin)) {
                    unlink($skin);
                }
            }
	    }
	}

    /**
     * @method skin_url
     */  
    static public function skin_url()
    {
    	$cache_dir = self::get_cache_dir();
    	$skin_id   = get_option(self::$skin_key);
    	
    	// No skin id, recompile the skin.
    	if(!$skin_id) {
        	self::compile_skin();
        	return self::skin_url();
    	}
    	
    	// Get the preview or skin filename.
    	if(self::is_preview()) {
    	    $filename = 'preview-' . $_GET['fl-preview'];
    	}
    	else {
    	    $filename = 'skin';
    	}
    	
    	// Check to see if the preview or skin exists. 
    	if(!file_exists($cache_dir['path'] . $filename . '-' . $skin_id . '.css')) {
	    	self::compile_skin();
        	return self::skin_url();
    	}
    	
    	// Return the url. 
    	return $cache_dir['url'] . $filename . '-' . $skin_id . '.css';
    }

	/**
     * @method compile_skin
     */		
	static public function compile_skin()
	{
	    $theme_info  = wp_get_theme();
		$css		 = '';
        $settings    = self::get_settings();
        $skin_file   = FL_THEME_DIR . '/less/skin-' . $settings->skin . '.less';
        $cache_dir 	 = self::get_cache_dir();
        $new_skin_id = uniqid();

        // Theme stylesheet
        $css .= file_get_contents(FL_THEME_DIR . '/less/theme.less');
        
        // WooCommerce
        $css .= file_get_contents(FL_THEME_DIR . '/less/woocommerce.less');
        
        // Skin
        if(file_exists($skin_file)) {
            $css .= file_get_contents($skin_file);
        }
        
        // Default Logo
        if($settings->logo_type == 'image' && empty($settings->logo_image)) {
            if($theme_info->Name == 'Beaver Builder Theme' || $theme_info->Name == '{FL_THEME_NAME}') {
                $css .= file_get_contents(FL_THEME_DIR . '/css/bb-logo.css');
            }
            else {
                $css .= file_get_contents(FL_THEME_DIR . '/css/automator-logo.css');
            }
        }
        
        // Replace {FL_THEME_URL} placeholder.
        $css = str_replace('{FL_THEME_URL}', FL_THEME_URL, $css);
        
        // Compile LESS
        $css = self::compile_less($css);
        
        // Compress
        $css = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $css);
        $css = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $css);
        
        // Get preview or live skin info.
        if(self::is_preview()) {
            $filename = 'preview-' . $_GET['fl-preview'];
        }
        else {            
            $filename = 'skin';
        }
            
        // Save the new skin.
        file_put_contents($cache_dir['path'] . $filename . '-' . $new_skin_id . '.css', $css);
        
        // Save the new skin id.
        update_option(self::$skin_key, $new_skin_id);
        
        // Return the filename.
        return $filename;
	}
    
    /**
     * @method compile_less
     */	
	static public function compile_less($css) 
	{
        require_once 'lessc.inc.php';
        
        $less = new lessc;
        $settings = self::get_settings();
        
        // Fix issue with IE filters
        $css = preg_replace_callback('(filter\s?:\s?(.*);)', 'FLAdmin::preg_replace_less', $css);
        
        // Mixins
        $mixins = file_get_contents(FL_THEME_DIR . '/less/mixins.less');
         
        // Vars
        $less_vars = self::get_settings_less_vars();
        
        // Compile and return
        return $less->compile($mixins . $less_vars . $css);
	}
    
    /**
     * @method get_settings_less_vars
     */	
	static public function get_settings_less_vars() 
	{
        $settings = self::get_settings();
        $vars = array();
        $vars_string = '';
        
        // Accent Color
        $vars['accent-color']                   = FLColor::hex($settings->accent_color);
        $vars['accent-fg-color']                = FLColor::foreground($settings->accent_color);
        
        // Text Colors
        $vars['heading-color']                  = FLColor::hex($settings->heading_color);
        $vars['text-color']                     = FLColor::hex($settings->text_color);
        
        // Fonts
        $vars['text-font']                      = '"' . $settings->text_font . '", sans-serif';
        $vars['heading-font']                   = '"' . $settings->heading_font . '", sans-serif';
        $vars['heading-weight']                 = $settings->heading_weight;
        $vars['logo-font']                   	= '"' . $settings->logo_font . '", sans-serif';
        $vars['logo-weight']                 	= $settings->logo_weight;
        $vars['logo-size']                 		= $settings->logo_size . 'px';
        
        // Body Background Image
        $custom 								= !empty($settings->bg_image) && $settings->layout == 'boxed';
        $vars['body-bg-image']                  = $custom ? 'url(' . $settings->bg_custom_image . ')' : 'none';
        $vars['body-bg-repeat']                 = $custom ? $settings->bg_repeat : 'no-repeat';
        $vars['body-bg-position']               = $custom ? $settings->bg_position : '0 0';
        $vars['body-bg-attachment']             = $custom ? $settings->bg_attachment : 'scroll';
        $vars['body-bg-size']                   = $custom ? $settings->bg_size : 'auto';
        
        // Content Background Colors
        $vars['body-bg-color'] 					= FLColor::hex($settings->bg_color);
        $vars['body-bg-color-2'] 				= FLColor::similar(array(5, 4, 13), $settings->bg_color);
        $vars['body-fg-color']					= FLColor::foreground($settings->bg_color);
        
        // Content Border Colors
        $vars['border-color'] 					= FLColor::similar(array(10, 9, 19), $settings->content_bg_color);
        $vars['border-color-2'] 				= FLColor::similar(array(20, 20, 30), $settings->content_bg_color);
        
        // Content Background Colors
        $vars['content-bg-color'] 				= FLColor::hex($settings->content_bg_color);
        $vars['content-bg-color-2'] 			= FLColor::similar(array(1, 4, 13), $settings->content_bg_color);
        $vars['content-bg-color-3'] 			= FLColor::similar(array(3, 9, 18), $settings->content_bg_color);
        $vars['content-fg-color']				= FLColor::foreground($settings->content_bg_color);
        
        // Top Bar Background Color
        $type 									= $settings->top_bar_bg_type;
        $custom_bg								= $settings->top_bar_bg_color;
		$vars['top-bar-bg-color'] 				= FLColor::section_bg($type, $vars['content-bg-color'], $custom_bg);
		$vars['top-bar-fg-color'] 				= FLColor::section_fg($type, $vars['body-bg-color'], $custom_bg);
        $vars['top-bar-bg-grad']                = $settings->top_bar_bg_grad ? 10 : 0;

        // Header Background Color
        $type 									= $settings->header_bg_type;
        $custom_bg								= $settings->header_bg_color;
		$vars['header-bg-color'] 				= FLColor::section_bg($type, $vars['content-bg-color'], $custom_bg);
		$vars['header-fg-color'] 				= FLColor::section_fg($type, $vars['body-bg-color'], $custom_bg);
        $vars['header-bg-grad']                 = $settings->header_bg_grad ? 10 : 0;
        
        // Fixed Header Background Color
        $vars['fixed-header-bg-color']      	= FLColor::hex(array($vars['header-bg-color'], $vars['body-bg-color']));
		$vars['fixed-header-fg-color']   		= FLColor::hex(array($vars['header-fg-color'], $vars['text-color']));
		$vars['fixed-header-accent-color']   	= FLColor::hex(array($vars['header-fg-color'], $vars['accent-color']));
		
		// Right Nav Colors
		if($settings->nav_position == 'right' || $settings->nav_bg_type == 'none') {
			$vars['nav-bg-color'] 				= $vars['header-bg-color'];
			$vars['nav-fg-color'] 				= $vars['header-fg-color'];
	        $vars['nav-bg-grad']                = 0;
		}
		
		// Bottom and Centered Nav Colors
		else {
	        $type 								= $settings->nav_bg_type;
	        $custom_bg							= $settings->nav_bg_color;
			$vars['nav-bg-color'] 				= FLColor::section_bg($type, $vars['content-bg-color'], $custom_bg);
			$vars['nav-fg-color'] 				= FLColor::section_fg($type, $vars['body-bg-color'], $custom_bg);
	        $vars['nav-bg-grad']                = $settings->nav_bg_grad ? 5 : 0;
		}
	        
        // Mobile Nav Background Color
        $vars['mobile-nav-bg-color']        	= FLColor::hex(array($vars['header-bg-color'], $vars['body-bg-color']));
        $vars['mobile-nav-bg-color']			= FLColor::similar(array(5, 10, 18), $vars['mobile-nav-bg-color']);
        $vars['mobile-nav-fg-color']        	= FLColor::hex(array($vars['header-fg-color'], $vars['text-color']));
		
        // Footer Widgets Background Color
        $type 									= $settings->footer_widgets_bg_type;
        $custom_bg								= $settings->footer_widgets_bg_color;
		$vars['footer-widgets-bg-color'] 		= FLColor::section_bg($type, $vars['content-bg-color'], $custom_bg);
		$vars['footer-widgets-fg-color'] 		= FLColor::section_fg($type, $vars['body-bg-color'], $custom_bg);
        
        // Footer Background Color
        $type 									= $settings->footer_bg_type;
        $custom_bg								= $settings->footer_bg_color;
		$vars['footer-bg-color'] 				= FLColor::section_bg($type, $vars['content-bg-color'], $custom_bg);
		$vars['footer-fg-color'] 				= FLColor::section_fg($type, $vars['body-bg-color'], $custom_bg);
        
        // WooCommerce
        if(self::is_plugin_active('woocommerce')) {
            $vars['woo-cats-add-button']        = $settings->woo_cats_add_button == 'hidden' ? 'none' : 'inline-block';
        }
        
        // Build the vars string
        foreach($vars as $key => $value) {
            $vars_string .= '@' . $key . ':' . $value . ';';
        }
        
        // Return the vars string
        return $vars_string;
	}
    
    /**
     * @method preg_replace_less
     */	
	static public function preg_replace_less($matches) 
	{
        if(!empty($matches[1])) {
            return 'filter: ~"' . $matches[1] . '";';
        }
        
        return $matches[0];
	}
    
    /**
     * @method export
     */	
	static public function export() 
	{
		header('Content-disposition: attachment; filename=theme-export.json');
		header('Content-type: application/json');
		
		$settings = self::get_settings();
		$settings->export_id = uniqid();
		
		echo json_encode($settings);
		
		die();
	}
    
    /**
     * @method get_import_settings
     */	
	static public function get_import_settings() 
	{
		$settings = json_decode(file_get_contents($_FILES['import_file']['tmp_name']));
		
		// Wrong type.
		if(gettype($settings) != 'object') {
    		self::$messages[] = array(
	        	'type'	  => 'error',
	        	'message' => __('ERROR: Please upload a theme export file.', 'fl-automator')
	        );
	        return false;
		}
		
		// No export id.
		if(!isset($settings->export_id)) {
			self::$messages[] = array(
	        	'type'	  => 'error',
	        	'message' => __('ERROR: The export file has been corrupted. Please export your settings and trying uploading them again.', 'fl-automator')
	        );
	        return false;
		}
        
        return $settings;
	}
	
	/**
     * @method is_plugin_active
     */	
	static public function is_plugin_active($slug)
	{
	   if(!function_exists('is_plugin_active')) {
            include_once(ABSPATH . 'wp-admin/includes/plugin.php');
        }
        
        return is_plugin_active($slug . '/' . $slug . '.php');
	}
	
	/**
     * @method get_cache_dir
     */	
	static public function get_cache_dir()
	{
        $wp_info = wp_upload_dir();
        
        $dir_info = array(
            'path'   => $wp_info['basedir'] . '/fl-automator/',
            'url'    => $wp_info['baseurl'] . '/fl-automator/'
        );
        
        // Create the cache dir if it doesn't exist.
        if(!file_exists($dir_info['path'])) {
            mkdir($dir_info['path']);
        }
        
        return $dir_info;
    }
}